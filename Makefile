.PHONY: all update update_revealjs download_d3js update_dagrejs update_angularplasmid

all: download_d3js 
update: update_revealjs update_dagrejs update_angularplasmid

download_d3js:
	wget -O public/wiring/d3.v5.js https://d3js.org/d3.v5.js 

update_revealjs:
	git fetch revealjs master
	git subtree pull --prefix public/wiring/reveal.js revealjs master --squash

update_dagrejs:
	git fetch dagrejs master
	git subtree pull --prefix public/wiring/dagre.js dagrejs master --squash

update_angularplasmid:
	git fetch angularplasmid master
	git subtree pull --prefix public/wiring/angularplasmid angularplasmid master --squash

update_reveald3:
	git fetch reveald3 master                                              
	git subtree pull --prefix public/wiring/reveald3 reveald3 master --squash

