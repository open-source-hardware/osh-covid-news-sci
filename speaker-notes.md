# title page
* Open source hardware during COVID-19
* and a work in progress subtitle: Classification is more like open washing

# Activity Time
Go to: https://pollev.com/snguyen

# Outline
* What is OSH?
* OSH in response to COVID-19
* Ventilators
* Medtronics
* Open washing
* Discussion & Questions
* Bibliography

# What is OSH?
* Now that you've taken a poll on what you all think might constitute as open source, here's the actual definition of open source hardware as stated by the open source hardware assocation (OSHWA)
* It's a physical, tangible artifact (i.e. hardware) accompanied by complementary materials, labeled under an accepted open license, that make the artifact accessible to anyone for inspection, use, modification, and redistribution (i.e. open source). 
* Many of these artifacts rely on digital and electronic technologies, but can also be totally analog 
* the actors in OSH include engineers, scientists, hobbyists, venture capitalists, makers and producers, and the people who use the hardware itself, what we most commonly refer to as users. These actors are not mutually exclusive. It is very common for the engineer to also the VC and the user
* OSH makers commonly use platforms such as GitHub or GitLab to share documentation but there many other OSH specific online community platforms such as the famous 3D printing forum, RepRap

# Development of OSH in response to COVID-19
* OSH in response to crises is not new. Due to 2011's Fukushima nuclear disaster, a non-profit organization developed an open source geiger counter device plus an open access logging system (software) which resulted in a massive citizen-science driven map of nuclear contamination in the region
* Global efforts to address the shortages of global and local equipment to address the needs to combat COVID. Ranging from PPE like DIY masks, to 3D printed parts and complete builds of ventilators, to diagnostic testing kits, and more. 
* WHY?
  - improving product innovation and manufacturing pipelines
  - fast & distributed development
  - nature of mandates to physically distance and shelter-in-place requires remote work which is a major characteristic of the open source movement where many people can contribute to a project remotely, relying on internet communication platforms to collaborate in a distributed fashion
  - lower implementation costs with OSH 
  - easy adaptability to local resources since open licenses allow people to remix and reuse to their own needs/accessibilities
  - any new hardware designs or improvements are then globally available. This is key because Different communities face different limitations in regards to bandwidth, trained staff, medical supplies or consumables, machines, instruments, and diagnostic tools so people can adjust tools to their local realities instead of being locked into a proprietary software or hardware system to measure something like a patient's oxygen intake 
  - OSH allows people to bypass traditional product chains to flexibly supply areas in need and respond to issues as the emerge
* So we can see that OSH does not just address the technical and scientific considerations to the medical equipment but is trying to be sensitive to the diverse economic, social, and cultural needs throughout this pandemic. 

sources:
* Safecast. In: Safecast [Internet]. [cited 20 Aug 2018]. Available: https://blog.safecast.org/

# Ventilators
* In this case, I'm focusing on ventilators. 
* For those who don't know, ventilators are, they are medical devices that support or take over entirely a patient's respiration by delivering air or other gas mixtures. This can be an external mask pumping airflow or a complex internal tub inserted into the patient's airway. 
  - Ventilator are needed when a person has difficulty breathing on their own (called respiratory failure) which is a common symptom of coronavirus
  - Improper use of ventilators can lead to infections such as pneumonia, lung damage, oxygen toxicity, excess air pressure, or too much concentated O2
* A scientic article (Chagas, 2020) notes that there are just under 30 OSH ventilators by the time of publishing their article in April 2020. But, as of September 2020, there are over 170 OSH ventilator projects tracked on a very public and widespread GitHub repository and Google Spreadsheet. 
* Here's a link to the public spreadsheet for makers to submit their OSH ventilator projects to share https://docs.google.com/spreadsheets/u/2/d/1inYw5H4RiL0AC_J9vPWzJxXCdlkMLPBRdPgEVKF8DZw/edit?usp=sharing
* Notice on the first tab, row 4, there's a "Link to definition of evaluation criteria". This is another public page to demonstrate transparency on how the moderators of this list evaluate OS ventilator projects. The moderators of the list are board members and employees for a non-profit called Public Invention. They're a mixed bag of academics, tech developers, a legal counselor, and a finance executive.
* So while there seems to be a team of smart people who might seem to show authority over the topic of OSH and ventilators, it was interesting to see how their evaluation page abides by the UK's official ventilator guidelines, but does not refer the US guidlines. While many of OSH projects have are known to be have international contributions, the non-profit hosting the lists and their staff are all Americans. 

Anyway, the specific case with ventilators that I will highlight that poses an STS wrinkle is that there is this one OSH released by Medtronic. 
* Medtronic is 71 year old private company who's mission statement is to make use of the power of medical technology to improve lives by alleviating pain, restoring health, and extending life. 
* They released design specifications for a basic ventilator model “to enable participants across industries to evaluate options for rapid ventilator manufacturing to help doctors and patients dealing with COVID-19.” 
* Immediately after release, makers signed up to access the documentation and found that this was not open source at all. Not only was there a slew of negativ reviews and backlash on the "lack of technical drawing files, parts sources, bill of materials, manufacturing tools, source suppliers, versions/revisions, and electronic parts" made it difficult for PhD trained design engineers to reproduce from what was given. Original CAD files (computer assisted design) were missing to render the design itself. Instead, Medtronic just shared shematics (graphics and symbols to represent the design) on PDF files which is not reproducible and one of the least open source file formats that exist!
* It was clear that this is just a PR announcement from the medical device supply corporation
* Most scientific articles about OSH hardware, and in particular ventilators, have an optimistic approach to sharing the many OSH projects and developments as if these community projects will solve the dire states we face during the pandemic. They would show off the new technology and production approach with little to no address to the scalability to these projects. While these projects may be seen as pilots to expand the knowledge base and spark further innovation that will eventually become scalable, there is, again, little to no criticism from the scientific-side on how the bottleneck in OSH is the biggest challenge. 
* Even a Tesla employee for Tesla who has been working on Tesla's open source medical device initiatives notes that "the [major] challenge  is more of a supply-chain issue and laborers,” (Ohnsman, 2020). That is the supply of the raw base parts such as the motherboard, arduinos, raspberri pi's, and the trained staff to actually produce the ventilators at scale. 
* Additionally, there's the issue with testing these OSH projects that allow varying components that may change the quality of the ventilators performance. How is this evaluated for saftey and effectiveness from the FDA or medical professionals when people are remixing and making variants from the original.
* so this follows Bowkers idea that the classification of an artifact being open source is not problematic but the system in which OSH operates and the awkward navigation that people operate around the OSH license become problematic in that it does not actually deliver a useable and scalable artifact

EXTRA
* When I find the time to dig into this research, I'd like to compare NIH to UK ventilator guidelines
* compare ventilator projects and if any appear on NIH 3d OS database
* how does each OSH project declare they abide by national criteria, or show testing validities
 
sources:
* https://www.gov.uk/government/publications/specification-for-ventilators-to-be-used-in-uk-hospitals-during-the-coronavirus-covid-19-outbreak
* Ventilator/Ventilator Support | National Heart, Lung, and Blood Institute (NHLBI). [cited 20 Mar 2020]. Available: https://www.nhlbi.nih.gov/health-topics/ventilatorventilator-support
* https://www.pubinv.org/project/ventilator-verification-project/
* Peters J. Volunteers produce 3D-printed valves for life-saving coronavirus treatments. In: The Verge [Internet]. 17 Mar 2020 [cited 20 Mar 2020]. Available: https://www.theverge.com/2020/3/17/
21184308/coronavirus-italy-medical-3d-print-valves-treatments
* ApolloBVM. In: Google Docs [Internet]. [cited 20 Mar 2020]. Available: https://docs.google.com/document/d/1-DRXnVkJOlDCmvTzh-DgWDxeLSrZTiBYyH0ypzv8tNA
* Lee J. jcl5m1/ventilator. 2020. Available: https://github.com/jcl5m1/ventilator
* panventFollow. The Pandemic Ventilator. In: Instructables [Internet]. [cited 21 Mar 2020]. Available: https://www.instructables.com/id/The-Pandemic-Ventilator/
* Trevor Smale / Open Source Ventilator—OpenLung BVM Ventilator. In: GitLab [Internet]. [cited 20 Mar 2020]. Available: https://gitlab.com/TrevorSmale/OSV-OpenLung
* O’Donnell R. RuairiSpain/openVentilator. 2020. Available: https://github.com/RuairiSpain/openVentilatorPubInv/covid19-vent-list. Public Invention; 2020. Available: https://github.com/PubInv/covid19-vent-list
* Corona COVID19 opensource. In: Google Docs [Internet]. [cited 23 Mar 2020]. Available: https://docs.google.com/document/d/1UH7AZWH3riztt1rTQzRHC-cZYGTPFPyavOxXcvadU9M
* Welcome to the OxyGEN project. In: OxyGEN [Internet]. 20 Mar 2020 [cited 20 Mar 2020]. Available: https://www.oxygen.protofy.xyz/
* coronamakers ventilator. [cited 23 Mar 2020]. Available: https://www.coronavirusmakers.org/index.php/es/prototipos/hardware
 
# Open washing
* So I'm interpreting the science and news articles about open source ventilators as a classification issue, in that the term "open source" creates a sense of authority and certification on specific projects which scientists are excited to share with the world to play the role of hero during the pandemic but it has been more common for news media outlets, who are interviewing the ancillary makers and the medical workers who are supposed to be the "users" of the OS ventilator, and they run into more challenges than hope.
  -so this follows Bowkers idea that the classification of an artifact being open source is not problematic but the system in which OSH operates and the awkward navigation that people operate around the OSH license become problematic in that it does not actually deliver a useable and scalable artifact
  - While these open source projects build upon existing projects as they remix and remake from the availability of documentation, it's only when thorough and detailed documentation is avilalable will OSH actually help with the shortage of medical supplies. Putting a OSH certificate or stamp on an object bu not providing any of the complementary documentation leads to what is called open-washing. It's like hygeine theater-- becomes a motion for good marketing but no tangible true delivery of the goods
* Scientific papers, such as Chagas (2020) have a Caution Disclaimer box after the Introduction and 2-pages into the article claiming that "it is important to critically evaluate the reliability and safety of the design on a case by case basis". And they warn readers that designs need to be "extensively tested" and that the authors have no formal medical training but base their findings on available literature. 
* I used to work for Macmillan Publishers, one of the big 5 traditional book publishers, and through years of reader surveys, everyone on the editorial and design team knew that it was rare for students to actually read the text within the boxed areas, particularly in textbooks. So I found the placement of these disclaimers particularly interesting.

# Discussion & Questions

I'm still trying to pinpoint the torque in this narrative. I believe it has something to do with the makers who publish their so-called OSH project but when their published works do not fit the necessary accessibility standards, such as all of the technical documentation, bill of materials, open file formats, clarified instructions, then the projects and the maker who released the project are stuck in this stagnant no-use, no growth when the very existence of OSH projects is that many contributors from distributed remote regions are continuously adding, testing, remixing, reusing parts of the project to keep it alive. 
Open to thoughts!
